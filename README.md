## Angular Tabs
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

Prepare Tabs

##### Example:
```
<tabs>
  <tab *ngFor="let tab of tabs">
    <tab-title>
      Tab <b>{{ tab }}</b> title
    </tab-title>
    <tab-content>
      Tab <b>{{ tab }}</b> content
      <!--<test [tab]="tab"></test>-->
    </tab-content>
  </tab>
</tabs>
```
##### Style Example:
```
.tabs__titles {
  flex: 1 1 0;
  flex-direction: row;
  margin-bottom: 10px;
}
.tabs__title {
  display: inline-block;
  padding: 5px;
  cursor: pointer;
}
.tabs__title--active {
  border-bottom: 2px solid #ff2222;
}
```

## Angular view port

This module uses resize event in service
and by directive applying visibility for objects

#### Default points:
`small`: <960

`medium`: 960<->1280,

`large`: >1280

#### Example:
   
```
<div *ifViewportSize="'small'" style="background: green">small</div>
<div *ifViewportSize="'medium'" style="background: yellow">medium</div>
<div *ifViewportSize="'large'" style="background: red">large</div>
```

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

