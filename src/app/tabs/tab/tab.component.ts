import {Component, ContentChild, Input, OnDestroy} from '@angular/core';
import {TabsComponent} from '../tabs.component';
import {TabTitleComponent} from '../tab-title/tab-title.component';

@Component({
  selector: 'tab',
  template: `<ng-content select="tab-content"></ng-content>`,
  host: {
    "[style.display]": "active ? 'block' : 'none'"
  },
})
export class TabComponent implements OnDestroy{
  @Input() active: boolean = false;
  @ContentChild(TabTitleComponent, {read: TabTitleComponent}) tabTitle;

  constructor(private tabs: TabsComponent) {
    tabs.addTabHeader(this);
  }
  ngOnDestroy() {
    this.tabs.removeTabHeader(this);
  }

  getTabTitleText() {
    return this.tabTitle.title;
  }
}
