import {AfterViewInit, Component} from '@angular/core';
import {TabComponent} from './tab/tab.component';

@Component({
  selector: 'tabs',
  template: `
    <div>
      <div class="tabs__titles">
        <div class="tabs__title"
             (click)="selectTab(tab)"
             [ngClass]="{'tabs__title--active' : tab.active}"
             *ngFor="let tab of tabs"
        >{{ tab.getTabTitleText() }}</div>
      </div>
      <div class="tab-content">
        <ng-content></ng-content>
      </div>
    </div>
  `
})
export class TabsComponent implements AfterViewInit{
  tabs: TabComponent[] = [];
  ifSelected: boolean = false;
  addTabHeader(tab: TabComponent) {
    this.tabs.push(tab);
    if (this.tabs.length === 1) {
      this.selectTab(this.tabs[0]);
    }
  }
  removeTabHeader(tab: TabComponent){
    if(tab.active) {
      this.selectTab(this.tabs[0]);
    }
    this.tabs = this.tabs.filter(t => t !== tab);
  }
  selectTab(tab: TabComponent) {
    this.tabs.forEach(_tab => {
      _tab.active = false;
    });
    tab.active = true;
  }
  ngAfterViewInit() {
    this.tabs.forEach(tab => {
      if (tab.active) {
        this.selectTab(tab);
        this.ifSelected = true;
      }
    });
    if (!this.ifSelected) {
      this.selectTab(this.tabs[0]);
    }
  }
}
