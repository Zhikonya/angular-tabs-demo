import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabsComponent } from './tabs.component';
import { TabComponent } from './tab/tab.component';
import { TabTitleComponent } from './tab-title/tab-title.component';
import { TabContentComponent } from './tab-content/tab-content.component';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [TabsComponent, TabTitleComponent, TabContentComponent, TabComponent],
  exports: [TabsComponent, TabTitleComponent, TabContentComponent, TabComponent]
})
export class TabsModule { }
