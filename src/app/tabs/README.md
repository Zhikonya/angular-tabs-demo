## Angular Tabs

Prepare Tabs

##### Example:
```
<tabs>
  <tab *ngFor="let tab of tabs">
    <tab-title>
      Tab <b>{{ tab }}</b> title
    </tab-title>
    <tab-content>
      Tab <b>{{ tab }}</b> content
      <!--<test [tab]="tab"></test>-->
    </tab-content>
  </tab>
</tabs>
```
##### Style Example:
```
.tabs__titles {
  flex: 1 1 0;
  flex-direction: row;
  margin-bottom: 10px;
}
.tabs__title {
  display: inline-block;
  padding: 5px;
  cursor: pointer;
}
.tabs__title--active {
  border-bottom: 2px solid #ff2222;
}
```
