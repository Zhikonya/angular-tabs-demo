import {Component, ViewChild} from '@angular/core';
import {ImageComponent} from './image/image.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  @ViewChild('tabContainer') container;
  public tabs = [
    {
      name: 'Tab 1',
      content: ImageComponent
    },
    {
      name: 'Tab 2',
      content: 'Content 2'
    },
    {
      name: 'Tab 3',
      content: 'Content 3'
    }
  ];
  public isString(content) {
    return (typeof content === 'string');
  }
  public dec() {
    const activeTabIndex = this.container.tabs.findIndex(tab => tab.active);
    this.tabs.splice(activeTabIndex, 1);
  }
  public inc() {
    const nextIndex = this.tabs.length + 1;
    this.tabs.push({name: `Tab ${nextIndex}`, content: `Content ${nextIndex}`});
  }
}
