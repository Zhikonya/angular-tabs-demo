import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ViewportDirective} from './directives/viewport.directive';
import {ViewportService} from './providers/viewport.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ViewportDirective],
  providers:[ViewportService],
  exports:[ViewportDirective],
})
export class ViewportModule { }
