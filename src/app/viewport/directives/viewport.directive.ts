import {Directive, Input, TemplateRef, ViewContainerRef} from '@angular/core';
import {ViewportService} from '../providers/viewport.service';

@Directive({
  selector: '[ifViewportSize]',
})
export class ViewportDirective {
  @Input() set ifViewportSize(value){
    this._size = value;
    this.checkVisibility();
  };

  private _size: string;
  private _shown: boolean = false;
  private _viewportSize: string;

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    private service: ViewportService
  ){
      this._viewportSize = service.currentViewport.value;
      service.currentViewport.subscribe(viewportSize=>{
        this._viewportSize = viewportSize;
        this.checkVisibility()
      });
  }

  checkVisibility = () => {
    if(this._size === this._viewportSize && !this._shown){
      this.viewContainer.createEmbeddedView(this.templateRef);
      this._shown = true;
    }else{
      this.viewContainer.clear();
      this._shown = false;
    }
  }
}
