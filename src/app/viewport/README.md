# Angular view port module

This module uses resize event in service
and by directive applying visibility for objects

## Default points:
`small`: <960

`medium`: 960<->1280,

`large`: >1280

## Example:
   
```
<div *ifViewportSize="'small'" style="background: green">small</div>
<div *ifViewportSize="'medium'" style="background: yellow">medium</div>
<div *ifViewportSize="'large'" style="background: red">large</div>
```
