import {Injectable, Renderer2, RendererFactory2} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {IConfig} from '../interfaces/IConfig';

const ViewportConfig: IConfig = {
  medium: 960,
  large: 1280
};


@Injectable({
  providedIn: 'root'
})
export class ViewportService {
  private renderer: Renderer2;

  public currentViewport = new BehaviorSubject<string>(null);

  widthHandler(width: number) {
    switch (true) {
      case (width < ViewportConfig.medium && this.currentViewport.value !== 'small'):
        this.currentViewport.next('small');
        break;
      case (width >= ViewportConfig.medium && width < ViewportConfig.large && this.currentViewport.value !== 'medium'):
        this.currentViewport.next('medium');
        break;
      case (width > ViewportConfig.large  && this.currentViewport.value !== 'large'):
        this.currentViewport.next('large');
        break;
    }
  }

  constructor(rendererFactory2: RendererFactory2) {
    this.renderer = rendererFactory2.createRenderer(null, null);
    this.renderer.listen('window', 'resize', event => this.widthHandler(event.target.innerWidth));
    this.widthHandler(window.innerWidth);
  }
}
