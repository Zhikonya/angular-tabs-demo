import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TabsModule } from './tabs/tabs.module';
import { ViewportModule } from './viewport/viewport.module';
import {ImageComponent} from './image/image.component';

@NgModule({
  declarations: [
    AppComponent,
    ImageComponent
  ],
  imports: [
    BrowserModule,
    TabsModule,
    ViewportModule
  ],
  // providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    ImageComponent
  ]
})
export class AppModule { }
